# Cybertech UI UX Exam

VS Code Tip: You can also right-click on the editor Tab and select Open Preview (Ctrl+Shift+V) or use the Command Palette (Ctrl+Shift+P) to run the Markdown: Open Preview to the Side command (Ctrl+K V).

## npm i
Run this command to install necessary node modules for development

## npm start
Run this command to start development


## sass --watch scss:css --style compressed
Run this command in assets folder to convert scss files to css minified


## gulp build
Run this command to get vendor folder


## Deployment
We can select only index html file together with css, js, vendor folders and deploy in live environment. Due to the time constrain I could not write a build task to minify and create a folder for distribution.

## Solution
Page is completely responsive
Images, scripts, stylesheets have been minified and optimized

## Funtionalities implemented
Responsive header,
Search popup,
When you click on product filter check boxes light blue title buttons are getting changed,
Products preview is available on click on the image.


## Copyright and License

Copyright 2013-2019 Blackrock Digital LLC. Code released under the [MIT](https://github.com/BlackrockDigital/startbootstrap-bare/blob/gh-pages/LICENSE) license.
