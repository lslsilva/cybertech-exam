$(function () {
  // variables
  $productFilterSelection = [];

  //header animation
  
  updateHeader = ()=>{
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
        $("header").addClass("scrolled");
    }
    else{    
      $("header").removeClass("scrolled"); 
    }
  }

  let throttled = _.throttle(updateHeader, 100);
  $(window).scroll(throttled); 
  
  // navigation toggle
  $(".navbar-toggler").on("click",()=>{
    $("body").toggleClass("menu-open");
  });

  // main slider
  $(".ct-product-banner").slick({
    speed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          arrows: false
        },
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false
        },
      },
    ],
  });

  // product filter
  getFilterItems = () => {
    $productFilterSelection = [];
    $(".ct-products-filter > ul > li > ul > li.selected").each(
      (index, element) => {
        $productFilterSelection.push($(element).text());
      }
    );
  };

  setSelectedItems = () => {
    $(".ct-products-selection > ul").html("");
    $productFilterSelection.map((element) => {
      $(".ct-products-selection > ul").append(`<li>${element}</li>`);
    });
  };
  // init
  $(".ct-products-filter > ul li > ul > li:first-of-type").addClass("selected");
  getFilterItems();
  setSelectedItems();

  $(".ct-products-filter > ul > li > ul > li").on("click", (event) => {
    $(event.currentTarget)
      .addClass("selected")
      .siblings()
      .removeClass("selected");
    getFilterItems();
    setSelectedItems();
  });
});
