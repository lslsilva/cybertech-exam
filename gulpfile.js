"use strict";

// Load plugins
const browsersync = require("browser-sync").create();
const del = require("del");
const gulp = require("gulp");
const merge = require("merge-stream");

// BrowserSync
function browserSync(done) {
  browsersync.init({
    server: {
      baseDir: "./",
    },
    port: 3000,
  });
  done();
}

// BrowserSync reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

// Clean vendor
function clean() {
  return del(["./assets/vendor/"]);
}

// Bring third party dependencies from node_modules into vendor directory
function modules() {
  // Bootstrap
  var bootstrap = gulp
    .src("./node_modules/bootstrap/dist/js/bootstrap.min.js")
    .pipe(gulp.dest("./assets/vendor/bootstrap"));
  // jQuery
  var jquery = gulp
    .src([
      "./node_modules/jquery/dist/jquery.min.js",
    ])
    .pipe(gulp.dest("./assets/vendor/jquery"));
  // slick carousel
  var slick = gulp
    .src([
      "./node_modules/slick-carousel/slick/slick.min.js",
    ])
    .pipe(gulp.dest("./assets/vendor/slick"));
  // fancybox
  var fancybox = gulp
    .src([
      "./node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js",
      "./node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css",
    ])
    .pipe(gulp.dest("./assets/vendor/fancybox"));
  // underscore
  var underscore = gulp
    .src([
      "./node_modules/underscore/underscore-min.js",
    ])
    .pipe(gulp.dest("./assets/vendor/underscore"));
  return merge(bootstrap, jquery, slick, fancybox, underscore);
}

// Watch files
function watchFiles() {
  gulp.watch("./**/*.css", browserSyncReload);
  gulp.watch("./**/*.html", browserSyncReload);
}

// Define complex tasks
const vendor = gulp.series(clean, modules);
const build = gulp.series(vendor);
const watch = gulp.series(build, gulp.parallel(watchFiles, browserSync));

// Export tasks
exports.clean = clean;
exports.vendor = vendor;
exports.build = build;
exports.watch = watch;
exports.default = build;
